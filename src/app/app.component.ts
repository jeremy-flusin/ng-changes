import { Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  bank: 'CA' | 'CE' | 'LCL' = 'CA';
  operation: 'withdraw' | 'payment' = 'withdraw';
  rate = 1.1825;
  amountLS = 0;
  amountEUR = 0;

  totalCost = 0;
  charges = 0;

  constructor(private http: HttpClient){
    this.http.get('https://api.exchangeratesapi.io/latest?base=GBP').subscribe((res: any) => {
      const rate = res.rates.EUR;
      if(rate && rate > 0){
        this.rate = rate;
      }
    });
  }

  compute = () => {
    if(this.amountLS && this.amountLS > 0){
      this.amountEUR = this.amountLS * this.rate;
      if(this.bank === 'CA'){
        const fixedCharges = ((this.operation === 'withdraw') ? 3.50 : 0.60);
        const variableCharges = this.amountEUR * ((this.operation === 'withdraw') ? 2.20 : 2.20) / 100;
        this.charges =  fixedCharges + variableCharges;
      }else if(this.bank === 'LCL'){
        const fixedCharges = ((this.operation === 'withdraw') ? 3.00 : 1.20);
        const variableCharges = this.amountEUR * ((this.operation === 'withdraw') ? 2.85 : 2.85) / 100;
        this.charges =  fixedCharges + variableCharges;
      }else if(this.bank === 'CE'){
        const fixedCharges = ((this.operation === 'withdraw') ? 3.20 : 0);
        const variableCharges = this.amountEUR * ((this.operation === 'withdraw') ? 2.80 : 2.85) / 100;
        this.charges =  fixedCharges + variableCharges;
      }
      this.totalCost = this.amountEUR + this.charges;
    }

  };

  setBank = (bank) => {
    this.bank = bank;
    this.compute();
  };

  setOperation = (operation) => {
    this.operation = operation;
    this.compute();
  };

}
