#!/bin/bash

npm install
ng build --aot --prod --base-href=/frais/ --deploy-url=https://flusin.ovh/frais/
docker-compose up -d
